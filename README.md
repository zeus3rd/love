## Build project

Usage:

./build.sh {projects folder name}

Output:

A .love file will be created under ./builds/{project}.love

Example:

```
./build.sh helloworld
```

## Run project (After building)

```
love ./builds/{project}.love
```

## Build and Run example

```
./build.sh helloworld && love ./dist/helloworld.love
```
