local player = {
	image: nil,
	frames: {
		-- walk: {},
	},
	state: 'walk',
	frame: 1,
	rotate: 0,
	scale_x: 1,
	scale_y: 1,
	origin_x: 0,
	origin_y: 0,

	x: 0,
	y: 0,
	load = function(self)
		self.image = love.graphics.newImage("DN1.png")

		self.frames.walk = {
			love.graphics.newQuad(0,0,8,16,imageFile:getDimensions())
			love.graphics.newQuad(8,0,8,16,imageFile:getDimensions())
			love.graphics.newQuad(16,0,8,16,imageFile:getDimensions())
			love.graphics.newQuad(24,0,8,16,imageFile:getDimensions())
		}
		-- self.frames.walk[1] = love.graphics.newQuad(0,0,8,16,imageFile:getDimensions())
		-- self.frames.walk[2] = love.graphics.newQuad(8,0,8,16,imageFile:getDimensions())
		-- self.frames.walk[3] = love.graphics.newQuad(16,0,8,16,imageFile:getDimensions())
		-- self.frames.walk[4] = love.graphics.newQuad(24,0,8,16,imageFile:getDimensions())
	end
	update = function(self)
		-- TODO: Frame timer
		self.frame += 1
		if self.frame > #self.frames[self.state] then
			self.frame=0
		end
	end
	draw = function(self)
		love.graphics.draw(self.image,
			self.frames[self.state][self.frame]),
			self.x,
			self.y,
			self.rotate,
			self.scale_x,
			self.scale_y,
			self.origin_x,
			self.origin_y,
		)
	end
}

function love.load()
	player:load()
end

function love.update()
	player:update()
end

function love.draw()
	player:draw()
end