if [ $# -eq 0 ]
  then
    echo "No project name supplied"
    exit 1
fi

SRC=./projects/$1/*
TARGET=./dist/$1.love

echo "Source: $SRC"
echo "Target: $TARGET"

rm $TARGET
zip -r -j $TARGET $SRC


# zip -r ../${PWD##*/}.love *